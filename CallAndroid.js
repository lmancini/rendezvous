/**
 * @providesModule CallAndroid
 */

'use strict';

/**
 * CallAndroid secret dialer
 */
var { NativeModules } = require('react-native');
module.exports = NativeModules.CallAndroid;
