package com.rendezvous;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONException;
import org.json.JSONObject;

public class PushNotificationReceiver extends ParsePushBroadcastReceiver {

    @Override
    protected void onPushReceive(Context context, Intent intent) {
        super.onPushReceive(context, intent);

        Bundle extras = intent.getExtras();
        if (extras == null) {
            return;
        }
        String intent_data = extras.getString("com.parse.Data");

        JSONObject jsonObject;
        String intent_payload;

        try {
            jsonObject = new JSONObject(intent_data);
            intent_payload = jsonObject.getString("payload");
        } catch (JSONException e) {
            return;
        }

        WritableMap wm = new WritableNativeMap();
        wm.putString("payload", intent_payload);

        this.sendEvent("pushReceived", wm);
    }

    private void sendEvent(String eventName,
                           @Nullable WritableMap params) {
        ReactContext reactContext = MainActivity.getActivity().getReactContext();
        reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }
}
