/**
 * @providesModule Parse
 */

'use strict';

/**
 * Parse interface
 */
var { NativeModules } = require('react-native');
module.exports = NativeModules.Parse;
