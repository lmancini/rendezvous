package com.rendezvous;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.parse.ParseInstallation;

public class ParseModule extends ReactContextBaseJavaModule {

    public ParseModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "Parse";
    }

    @ReactMethod
    public void put(String key, String value) {
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put(key, value);
        installation.saveInBackground();
    }
}
