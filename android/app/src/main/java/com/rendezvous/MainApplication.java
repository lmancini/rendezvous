package com.rendezvous;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseInstallation;

public class MainApplication extends Application {

    private static boolean mParseInitialized = false;

    @Override
    public void onCreate() {
        super.onCreate();

        if (!mParseInitialized) {
            // Enable Local Datastore.
            Parse.enableLocalDatastore(this);

            Parse.initialize(this, ParseKeys.ApplicationId, ParseKeys.ClientKey);

            ParseInstallation installation = ParseInstallation.getCurrentInstallation();
            installation.saveInBackground();

            mParseInitialized = true;
        }
    }
}
