'use strict';

var Q = require("q");

var _REMOTE = "http://trails.develer.net:8000/";
var _wallpapers = [];

var _list = function() {
    var deferred = Q.defer();
    var xhr = new XMLHttpRequest();
    xhr.open("GET", _REMOTE);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4) {
            _wallpapers.length = 0;
            if (xhr.status == 200) {
                var raw = xhr.responseText;
                var r = new RegExp("href=\"([^\"]+)\"", "g");
                while (true) {
                    var match = r.exec(raw);
                    if (!match) {
                        break;
                    }
                    _wallpapers.push(_REMOTE + match[1]);
                }
            }
            deferred.resolve();
        };
    }
    xhr.send();

    return deferred.promise;
};

module.exports = {
    choose: function() {
        var p;
        if (_wallpapers.length == 0) {
            p = _list();
        }
        else {
            p = Q.fcall(function() {});
        }
        return p.then(function() {
                if (_wallpapers.length == 0) {
                    return null;
                }
                return _wallpapers[Math.floor(Math.random() * _wallpapers.length)];
            });
    }
};
