// vim: ts=2:sw=2:sts=2
/**
 * Rendez-vous, threads-based app
 */
'use strict';

var React = require('react-native');

// Android extension modules
// var Parse = require('Parse');
var ThreadsModule = require('./threads-client-js/dist/react/threads_client.js');

// To receive push notification events
var RCTDeviceEventEmitter = require('RCTDeviceEventEmitter');
var Subscribable = require('Subscribable');

var W = require("./Wallpapers");

var {
  AlertIOS,
  AppRegistry,
  AsyncStorage,
  Event,
  Image,
  LinkingIOS,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  View,
} = React;

var THREADS_SERVER = 'http://trails.develer.net:9000/';


var ThreadsClientClass = ThreadsModule.reactNativeClient();

var Rendezvous = React.createClass({

  // Subscribable mixin to receive push notification events
  mixins: [Subscribable.Mixin],

  componentWillMount: function() {
    this.addListenerOn(RCTDeviceEventEmitter,
                       'pushReceived',
                       this.pushWasReceived);
  },

  pushWasReceived: function(evt) {
    var feedback_url = evt.payload;
    this.setState({"pending_feedback": feedback_url});
    AsyncStorage.setItem("pending_feedback", feedback_url);
  },
  //

  componentDidMount: function () {
    // Debug code to quickly enter "give a feedback state"
    /*
    this.setState({"pending_feedback": "/v1beta/threads/5068883707334290175"});
    this.client.AuthCode("1981794507870055588", "Client_4040789291349468906");
    this.client.Save()
      .done();
    */
  },

  getInitialState: function() {
    console.log("react, initial state");
    W.choose()
      .then((w) => {
        this.setState({"wallpaper": w});
       });
    this.client = new ThreadsClientClass(THREADS_SERVER);

    this.client.Load()
        .then((result) => {
            this.setState({"client_state": this.client.Status()});
            if (result) {
                console.log("session resumption:", this.client.ClientName());
            }
        })
        .done();

    // Load from storage an eventual feedback URL still to be left
    AsyncStorage.getItem("pending_feedback").then((value) => {
      this.setState({"pending_feedback": value});
    }).done();

    return {
      client_state: undefined
    };
  },

  _clearPendingFeedback: function() {
    console.log("_clearPendingFeedback");
    this.setState({"pending_feedback": null});
    this.setState({"giving_second_chance": false});
  },

  _openSecondChance: function() {
    console.log("_openSecondChance");
    this.setState({"giving_second_chance": true});

    // Read current second chance text to populate the widget
    this.client.Profile()
      .then(
        (result) => {
          var second_chance_text = result.data.second_chance;
          this.setState({"second_chance_profile_text": second_chance_text});
        },
        (reason) => {
          AlertIOS.alert("Error", "Unable to fetch remote profile");
          console.log(reason);
        }
      ).done();
  },

  _onSecondChanceInputChange: function(evt) {
    // Listen for second chance text events and update our local copy
    console.log(evt.nativeEvent.text);
    this.setState({"second_chance_profile_text": evt.nativeEvent.text});
  },

  _updateSecondChanceProfile: function(evt) {
    // Upload the new second chance text, then proceed to send second chance
    console.log("_updateSecondChanceProfile");

    this.client.Profile({"second_chance": this.state.second_chance_profile_text})
      .then(
        (result) => {
          this._sendSecondChance();
        },
        (reason) => {
          AlertIOS.alert("Error", "Unable to upload the second chance text");
          console.log(reason);
        }
      ).done();
  },

  _sendSecondChance: function() {
    // Actually send second chance as positive feedback
    console.log("_sendSecondChance");

    // Extract thread id from post notification payload
    // TODO: it probably makes more sense, for the web client as well, to pass
    // the thread id directly.
    var thread_id = this.state.pending_feedback.split('/').pop();

    this.client.ThreadFeedback(thread_id, {
      "feedback": "+1"
    }).then(

      (result) => {
        this._clearPendingFeedback();
      },

      (reason) => {
        AlertIOS.alert("Error", "Unable to send the second chance");
        console.log(reason);
      }
    ).done();
  },

  _leaveNegativeFeedback: function() {
    // Leave a negative feedback
    console.log("_leaveNegativeFeedback");

    var thread_id = this.state.pending_feedback.split('/').pop();

    this.client.ThreadFeedback(thread_id, {
      "feedback": "-1"
    }).then(

      (result) => {
        this._clearPendingFeedback();
      },

      (reason) => {
        AlertIOS.alert("Error", "Unable to leave negative feedback");
        console.log(reason);
      }
    ).done();
  },

  _requestSession: function(evt) {
    var telephone_number = evt.nativeEvent.text;
    console.log("requesting a new session for:", telephone_number);
    this.client.NewSession(telephone_number, "physical")
      .then((session) => {
        console.log("new session (not validated).", session.id, session.client);

        // In addition to the session ID, also save the user's telephone
        // number, which is not yet confirmed, but will be so when we are
        // granted an auth_code.
        this.setState({
          "client_state": "_pending",
          "session_id": session.id,
          "client_name": session.client,
          "telephone_number": telephone_number
        });
      });
  },

  _requestAuthCode: function(evt) {
    var sms_code = evt.nativeEvent.text;
    var session_id = this.state.session_id;
    var client = this.client;

    console.log("try to validate the session.", session_id, sms_code);
    client.ValidateSession(session_id, sms_code)
      .then((data) => {
        client.AuthCode(data.authcode, this.state.client_name);
        client.Save()
          .done();

        console.log("session validated! ready to go.", client.Status());

        // Now that we have an auth code, we can safely track this
        // installation's telephone number in Parse
        AsyncStorage.setItem("telephone", this.state.telephone_number)
          .then(() => {
            this._updateParseInstallation();
          });
        this.setState({client_state: client.Status()});
      });
  },

  _updateParseInstallation: function() {
    AsyncStorage.getItem("telephone")
      .then((value) => {
        console.log("parse init telephone number.", value);
        // Parse.put("telephone", value);
      })
      .done();
  },

  _startThread: function() {
    console.log("request a new thread");
    this.client.NewThread()
      .then(

        (result) => {
          console.log("new thread allocated.", result.id);
          var endpoint = result.endpoint.split(",")[0];
          LinkingIOS.openURL('tel:' + endpoint);
        },

        (reason) => {
          AlertIOS.alert("Error", "Cannot find a peer to connect");
          console.log(reason);
        }
      ).done();
  },

  render: function() {
    console.log("RENDER STATE>", this.state.client_state);
    console.log("RENDER STATE> giving second chance", this.state.giving_second_chance);
    console.log("RENDER STATE> pending_feedback", this.state.pending_feedback);
    console.log("RENDER STATE> second_chance_profile_text", this.state.second_chance_profile_text);

    var client_state = this.state.client_state;
    var pending_feedback = this.state.pending_feedback;
    var giving_second_chance = this.state.giving_second_chance;

    var bkg = this.state.wallpaper ? {uri: this.state.wallpaper} : require('image!background');
    switch (client_state) {
      case "connected":

        if (pending_feedback === null) {
          // Ready to have a Rendez-vous
          return (
            <View style={styles.container}>
              <Image
                source={bkg}
                style={[styles.backgroundImageBottomContent]}>

                <TouchableHighlight
                  style={styles.button}
                  onPress={this._startThread}>

                  <Text style={styles.buttonText}>Rendez-vous!</Text>

                </TouchableHighlight>

              </Image>
            </View>
          );
        } else {

        // We are in pending feedback mode. The user is expected to either not
        // leave any feedback, or give the other user a second chance.

        if (giving_second_chance !== true) {
          // First feedback page
          return (
            <View style={styles.container}>
              <Image
                source={bkg}
                style={styles.backgroundImage}>

                <View style={styles.dimmed}>
                  <Text style={styles.welcome}>
                    We hope the last call was great!
                  </Text>

                  <Text style={styles.instructions}>
                    This is your one occasion
                  </Text>

                  <Text style={styles.instructions}>
                    to give your pal a second chance.
                  </Text>

                  <TouchableHighlight
                    style={styles.button}
                    onPress={this._openSecondChance}>

                    <Text style={styles.buttonText}>Of course!</Text>

                  </TouchableHighlight>

                  <TouchableHighlight
                    style={styles.button}
                    onPress={this._clearPendingFeedback}>

                    <Text style={styles.buttonText}>Not interested</Text>

                  </TouchableHighlight>

                  <TouchableHighlight
                    style={styles.smallButton}
                    onPress={this._leaveNegativeFeedback}>

                    <Text style={styles.smallButtonText}>Report user</Text>

                  </TouchableHighlight>
                </View>

              </Image>
            </View>
          );
        } else {

          // Giving the other end a second chance (FIXME: prepolulate the text
          // input with the standard second chance).
          return (
            <View style={styles.container}>
              <Image
                source={bkg}
                style={styles.backgroundImage}>

                <View style={styles.dimmed}>
                  <Text style={styles.welcome}>
                    You are giving a second chance
                  </Text>

                  <TextInput style={styles.textinputmultiline}
                    multiline={true}
                    onChange={this._onSecondChanceInputChange}
                    value={this.state.second_chance_profile_text}
                  />

                  <TouchableHighlight
                    style={styles.button}
                    onPress={this._updateSecondChanceProfile}>

                    <Text style={styles.buttonText}>Send</Text>

                  </TouchableHighlight>
                </View>

              </Image>
            </View>
          );
        }
      }

      case "unknown":
        // No auth code yet, we have to create a session
        return (
            <View style={styles.container}>
            <Image
                source={bkg}
                style={styles.backgroundImage}>

                <View style={styles.dimmed}>
                  <Text style={styles.welcome}>
                  Welcome to Rendezvous.
                  </Text>
                  <Text style={styles.welcome}>
                  Enter your telephone number to start.
                  </Text>
                  <Text style={styles.instructions}>
                  (rest assured, we hate spam as much as you do)
                  </Text>
                  <TextInput style={styles.textinput}
                  onSubmitEditing={this._requestSession}
                  keyboardType='numeric'
                  defaultValue='+39'
                  />
                </View>

            </Image>
            </View>
        );
      case "_pending":
        // We have a session id, we need to enter the verification SMS code
        // note this state is set by this class not the ThreadsClient
        return (
          <View style={styles.container}>
            <Image
              source={bkg}
              style={styles.backgroundImage}>

              <View style={styles.dimmed}>
                <Text style={styles.welcome}>
                  Welcome to Rendezvous.
                </Text>
                <Text style={styles.instructions}>
                  Insert your SMS confirmation code
                </Text>
                <TextInput style={styles.textinput}
                  onSubmitEditing={this._requestAuthCode}
                  keyboardType='numeric'
                />
              </View>

            </Image>
          </View>
        );
      default:
        // no state, probably the ThreadsClient.Load() method is not complete yet.
        return (
          <View style={styles.container}>
            <Image
              source={bkg}
              style={styles.backgroundImage}>

              <View style={styles.dimmed}>
                <Text style={styles.welcome}>
                  Welcome to Rendezvous.
                </Text>
              </View>
            </Image>
          </View>
        );
    }
  }
});

var styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    color: '#F5FCFF',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#F5FCFF',
    marginBottom: 5,
  },

  textinput: {
    width: 200,
    height: 50,
    backgroundColor: '#ffffff',
    borderWidth: 1,
    fontSize: 20,
  },

  textinputmultiline: {
    width: 200,
    height: 200,
    color: '#F5FCFF',
    fontSize: 20,
  },

  // Standard button
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "rgba(0,0,0,0.6)",
    width: 200,
    height: 90,
    borderWidth: 3,
    borderRadius: 30,
    borderColor: "#F5FCFF",
    margin: 20,
  },

  buttonText: {
    color: "#F5FCFF",
    fontSize: 20,
  },

  // Negative feedback button
  smallButton: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "rgba(0,0,0,0.6)",
    width: 100,
    height: 45,
    borderWidth: 1,
    borderRadius: 30,
    borderColor: "#F5FCFF",
    margin: 30,
  },
  smallButtonText: {
    color: "#F5FCFF",
    fontSize: 10,
  },

  backgroundImage: {
    flex: 1,
    width: 600,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },

  backgroundImageBottomContent: {
    flex: 1,
    width: 600,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },

  dimmed: {
    backgroundColor: "rgba(0,0,0,0.85)",
    alignItems: 'center',
    borderRadius: 30,
    padding: 20,
  }
});

AppRegistry.registerComponent('Rendezvous', () => Rendezvous);
